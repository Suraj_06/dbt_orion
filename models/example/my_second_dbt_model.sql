
-- Use the `ref` function to select from other models


with src_demo as (

	select DeptName, sum(Salary) from prac.department as dept
	JOIN prac.EMPLOYEE as emp
	on emp.DEPTCODE= dept.DEPTCODE
	Group by DeptName
)

select * from src_demo